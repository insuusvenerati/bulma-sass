/**
 * Module Dependencies
 */

/* eslint-disable */
const gulp = require("gulp");
const babel = require("gulp-babel");
const sass = require("gulp-sass");
const del = require("del");
const sourcemaps = require("gulp-sourcemaps");
const autoprefixer = require("gulp-autoprefixer");
const browserSync = require("browser-sync");
const reload = browserSync.reload;
const nodemon = require("gulp-nodemon");
const minify = require("gulp-minify");
const babelBrowserConf = require("./.babelrc.browser");

gulp.task("js:clean", () => {
  return del("public/js/*");
});

gulp.task("js", ["js:clean"], () => {
  gulp
    .src("src/js/*.js")
    .pipe(babel(babelBrowserConf))
    .pipe(gulp.dest("./public/js"));
});

gulp.task("js:prod", ["js:clean"], () => {
  gulp
    .src("src/js/main.js")
    .pipe(babel(babelBrowserConf))
    .pipe(
      minify({
        ext: {
          min: ".js"
        }
      })
    )
    .pipe(gulp.dest("./public/js"));
});

gulp.task("css:clean", () => {
  return del("public/css/*");
});

gulp.task("css", ["css:clean"], () => {
  gulp
    .src("src/scss/main.scss")
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("./public/css"));
});

gulp.task("css:prod", ["css:clean"], () => {
  gulp
    .src("src/scss/main.scss")
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(gulp.dest("./public/css"));
});

gulp.task("nodemon", function(cb) {
  var called = false;
  return nodemon({
    script: "src/server/index.js",
    signal: "SIGTERM",
    watch: "src/server",
    ignore: ["*.test.js"],
    execMap: {
      js: "babel-node"
    }
  })
    .on("start", function() {
      if (!called) {
        called = true;
        cb();
      }
    })
    .on("restart", function() {
      setTimeout(function() {
        reload({ stream: false });
      }, 1000);
    });
});

gulp.task("browser-sync", ["nodemon"], function() {
  browserSync({
    proxy: "localhost:3000", // local node app address
    port: 5000, // use *different* port than above
    notify: true
  });
});

gulp.task("watch", ["browser-sync", "css", "js"], function() {
  gulp.watch(["src/scss/**/*.scss", "src/js/**/*.js"], ["js", "css"]);
  gulp.watch(['public/**/*', 'views/**/*'], reload);
});

gulp.task("build", ["css:prod", "js:prod"]);
