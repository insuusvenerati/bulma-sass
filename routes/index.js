var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Treats!'
  });
});

router.get('/contact', function (req, res, next) {
  res.render('contact', {
    title: 'Contact'
  })
})

router.get('/about', function (req, res, next) {
  res.render('about', {
    title: 'About'
  })
})

router.post('/submit', function (req, res, next) {
  console.log('Success');
  res.redirect('/')
})

router.get('/gallery', function(req, res, next) {
  res.render('gallery')
})

module.exports = router;
